define ([
	'backbone'
	], function(Backbone){
        var GlobalEvents = {};
        _.extend(GlobalEvents, Backbone.Events);
        console.log("global events loaded");
        return GlobalEvents;
	});