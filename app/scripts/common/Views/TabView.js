define ([
	'backbone',
	'underscore',
	'Events',
	'text!common/Templates/TabTemplate.html'
	], function(Backbone, _, GlobalEvents, TabTemplate){
		var TabView = Backbone.View.extend({
			events : {
				"click" : "clickTabHnadler"
			},

			selectors : {
				"number" : ".number",
				"tab"	 : ".tab"
			},

			template : _.template(TabTemplate),

			render : function (options) {
				this.$el.html(this.template(options));
				return this;
			},

			clickTabHnadler : function (e) {
				var tabType = $(e.target).attr("data-type") || $(e.target.parentElement).attr("data-type");
				GlobalEvents.trigger("navigate-"+tabType);
			},

			highlight : function (flag) {
				this.$(this.selectors.tab).toggleClass("active", flag);
			}

		});
		
		console.log("TabView loaded");
		return TabView;
	});