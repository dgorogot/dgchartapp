/*global require*/
'use strict';

require.config({
    shim: {
        underscore: {
            exports: '_'
        },
        backbone: {
            deps: [
                'underscore',
                'jquery'
            ],
            exports: 'Backbone'
        }
    },
    paths: {
        jquery     : '../bower_components/jquery/jquery',
        backbone   : '../bower_components/backbone/backbone',
        underscore : '../bower_components/underscore/underscore',
        highcharts : '../bower_components/highcharts-release/highcharts',
        text       : '../bower_components/requirejs-text/text'
    }
});

require([
    'App',
    'backbone'
], function (App, Backbone) {
    var DGApp = new App();
    Backbone.history.start();
    console.log('mainjs loaded');
});
