define ([
	'backbone',
    'Events',
	'modules/aside/AsideModule',
	'modules/charts/ChartsModule',
    'modules/about/AboutModule',
    'modules/details/DetailsModule'
	], function(Backbone, GlobalEvents, AsideModule, ChartsModule, AboutModule, DetailsModule){
        var DGApp = Backbone.Router.extend({
        	modules : {},

        	routes : {
                ""  	       : "home",
                "about"        : "about",
                "details/:CId" : "chartDetails",
            },

            home : function () {
                this.modules.about.hide();
                this.modules.details.hide();
                this.modules.charts.show();
            },

            about : function () {
                this.modules.about.show();
                this.modules.details.hide();
                this.modules.charts.hide();
            },

            chartDetails : function (chartId) {
                this.modules.about.hide();
                this.modules.charts.hide();
                this.modules.details.show(chartId);
            },


            initialize : function () {
                this.addEventListeners();
                this.loadModules();
            },

            addEventListeners : function () {
                GlobalEvents.on("navigate-home", this.navigateHome.bind(this));
                GlobalEvents.on("navigate-about", this.navigateAbout.bind(this));
                GlobalEvents.on("navigate-details", this.navigateDetails.bind(this));
            },

            navigateHome : function () {
                this.navigate("", {trigger: true});
            },

            navigateAbout : function () {
                this.navigate("about", {trigger: true});
            },

            navigateDetails : function (CId) {
                this.navigate("details/"+CId, {trigger: true});
            },

            loadModules : function () {
                this.modules.aside   = new AsideModule();
                this.modules.charts  = new ChartsModule();
                this.modules.about   = new AboutModule();
                this.modules.details = new DetailsModule();
            }

        });
        console.log("app loaded");
        return DGApp;
	});