define ([
	'backbone',
	'underscore',
	'Events',
	'text!modules/details/Templates/DetailTemplate.html'
	], function(Backbone, _, GlobalEvents, DetailTemplate){
		var DetailView = Backbone.View.extend({
			template : _.template(DetailTemplate),

			events : {
				"click .back-to-home"  : "backToHomeHandler",
				"click .edit-chart"    : "toggleEditFormHandler",
				"click .cancel-change" : "hideEditFormHandler",
				"click .apply-change"  : "applyChangesHandler"
			},

			selectors : {
				"chartPlaceholder" : ".detail-chart-placeholder",
				"editForm" 		   : ".edit-form",
				"chartName" 	   : ".chart-name"
			},

			initialize : function () {
				this.addHandlers();
			},

			addHandlers : function () {
				this.listenTo(this.model, "sync change", this.render);
			},

			render : function () {
				var options = {
					name		: this.model.attributes.chartParams.title.text,
					description : this.model.get("description")
				};
				this.$el.html(this.template(options));
				this.addChart();
				this.show();
			},

			backToHomeHandler : function () {
				GlobalEvents.trigger("navigate-home");
			},

			addChart : function () {
				var options = this.model.get("chartParams");
				this.chart = this.$(this.selectors.chartPlaceholder).highcharts(options);
			},

			toggleEditFormHandler : function () {
				this.$(this.selectors.editForm).toggleClass("hide");
			},

			hideEditFormHandler : function () {
				this.$(this.selectors.editForm).addClass("hide");
			},

			applyChangesHandler : function () {
				var nameVal = this.$(this.selectors.chartName).val();
				this.model.attributes.chartParams.title.text = nameVal;
				this.model.trigger("change");
			},

			hide : function () {
				this.$el.hide();
			},

			show : function () {
				this.$el.show();
			}

		});
		
		console.log("DetailView loaded");
		return DetailView;
	});