define ([
	'jquery',
	'backbone',
	'modules/details/Views/DetailView',
	'modules/details/Models/DetailModel'
	], function($, Backbone, DetailView, DetailModel){
		var DetailModule = Backbone.View.extend({
			views  : {},
			models : {},

			initialize : function () {
				this.initModels();
				this.initViews();
			},

			initModels : function () {
				this.models.detailModel = new DetailModel();
			},

			initViews : function () {
				this.views.detailView = new DetailView ({
					el 	  : $('.detail-placeholder'),
					model : this.models.detailModel
				});
			},

			show : function (chartId) {
				this.models.detailModel.setUrl(chartId).fetch();
			},

			hide : function () {
				this.views.detailView.hide();
			}

		});

		console.log("DetailModule loaded");
		return DetailModule;
	});