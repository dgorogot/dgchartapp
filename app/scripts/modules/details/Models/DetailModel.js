define ([
	'backbone',
	], function(Backbone){
		var DetailModel = Backbone.Model.extend({
			setUrl : function (chartId) {
				this.url = "data/chart"+chartId+".json";
				return this;
			}
		});
		
		console.log("DetailModel loaded");
		return DetailModel;
	});