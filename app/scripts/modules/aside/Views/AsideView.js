define ([
	'backbone',
	'underscore',
	'text!modules/aside/Templates/AsideTemplate.html'
	], function(Backbone, _, AsideTemplate){
		var AsideView = Backbone.View.extend({
			template : _.template(AsideTemplate),

			initialize : function () {
				this.render();
			},

			render : function () {
				this.$el.html(this.template());
			}

		});
		
		console.log("AsideView loaded");
		return AsideView;
	});