define ([
	'jquery',
	'backbone',
	'modules/aside/Views/AsideView'
	], function($, Backbone, AsideView){
		var AsideModule = Backbone.View.extend({
			views : {},

			initialize : function () {
				this.initViews();
			},

			initViews : function () {
				this.views.asideView = new AsideView ({el : $('.aside-placeholder')});
			}

		});

		console.log("AsideModule loaded");
		return AsideModule;
	});