define ([
	'backbone',
	'underscore',
	'text!modules/about/Templates/AboutTemplate.html'
	], function(Backbone, _, AboutTemplate){
		var AboutView = Backbone.View.extend({
			template : _.template(AboutTemplate),

			initialize : function () {
				this.addHandlers();
			},

			addHandlers : function () {
				this.listenTo(this.model, "sync", this.render);
			},

			render : function () {
				var options = {description : this.model.get("description")};
				this.$el.html(this.template(options));
				this.show();
			},

			hide : function () {
				this.$el.hide();
			},

			show : function () {
				this.$el.show();
			}

		});
		
		console.log("AboutView loaded");
		return AboutView;
	});