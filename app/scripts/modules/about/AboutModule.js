define ([
	'jquery',
	'backbone',
	'modules/about/Views/AboutView',
	'modules/about/Views/TabView',
	'modules/about/Models/AboutModel'
	], function($, Backbone, AboutView, TabView, AboutModel){
		var AboutModule = Backbone.View.extend({
			views  : {},
			models : {},

			initialize : function () {
				this.initModels();
				this.initViews();
				this.renderTab();
			},

			initModels : function () {
				this.models.aboutModel = new AboutModel();
			},

			initViews : function () {
				this.views.aboutView = new AboutView ({
					el 	  : $('.about-placeholder'),
					model : this.models.aboutModel
				});
				this.views.tabView = new TabView ();
			},

			renderTab : function () {
				var options = {
					"type"   : "about",
					"name"   : "about"
				};
				$('.aside-content').append(this.views.tabView.render(options).$el);
			},

			show : function () {
				this.models.aboutModel.fetch();
				this.views.tabView.highlight(true);
			},

			hide : function () {
				this.views.aboutView.hide();
				this.views.tabView.highlight(false);
			}

		});

		console.log("AboutModule loaded");
		return AboutModule;
	});