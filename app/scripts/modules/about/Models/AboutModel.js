define ([
	'backbone',
	], function(Backbone){
		var AboutModel = Backbone.Model.extend({
			url   : 'data/about.json'
		});
		
		console.log("AboutModel loaded");
		return AboutModel;
	});