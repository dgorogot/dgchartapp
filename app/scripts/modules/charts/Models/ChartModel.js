define ([
	'backbone',
	'underscore'
	], function(Backbone, _){
		var ChartModel = Backbone.Model.extend({
			defaults : {
				"name" 		  : "",
				"description" : ""
			}

		});
		
		console.log("ChartsCollection loaded");
		return ChartModel;
	});