define ([
	'backbone',
	'underscore',
	'modules/charts/Models/ChartModel'
	], function(Backbone, _, ChartModel){
		var ChartsCollection = Backbone.Collection.extend({
			url   : 'data/charts.json',
			model : ChartModel,
			

			parse : function (response) {
				return response;
			}
		});
		
		console.log("ChartsCollection loaded");
		return ChartsCollection;
	});