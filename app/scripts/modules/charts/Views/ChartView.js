define ([
	'backbone',
	'underscore',
	'highcharts',
	'Events',
	'text!modules/charts/Templates/ChartTemplate.html'
	
	], function(Backbone, _, highcharts, GlobalEvents, ChartTemplate){
		var ChartView = Backbone.View.extend({
			selectors : {
				"chartPlaceholder" : ".chart-placeholder"
			},

			events : {
				"click" : "expandChartHandler"
			},

			template : _.template(ChartTemplate),

			render : function () {
				var options = {
					"name" 		  : this.model.get("name"),
					"description" : this.model.get("description")
				}
				this.$el.html(this.template(options));
				this.addChart();
				return this;
			},

			expandChartHandler : function () {
				var chartId = this.model.get("id");
				GlobalEvents.trigger("navigate-details", chartId);
			},

			addChart : function () {
				var options = this.model.get("chartParams");
				this.chart = this.$(this.selectors.chartPlaceholder).highcharts(options);
			}

		});
		
		console.log("ChartView loaded");
		return ChartView;
	});