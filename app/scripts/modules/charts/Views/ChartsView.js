define ([
	'backbone',
	'underscore',
	'modules/charts/Views/ChartView',
	'text!modules/charts/Templates/ChartsTemplate.html'
	], function(Backbone, _, ChartView, ChartsTemplate){
		var ChartsView = Backbone.View.extend({
			template : _.template(ChartsTemplate),

			selectors : {
				"chartsContent" : '.charts-content'
			},

			initialize : function () {
				this.addHandlers();
			},

			addHandlers : function () {
				this.listenTo(this.collection, "sync", this.render);
			},

			render : function () {
				this.$el.html(this.template());
				this.renderItems();
				this.show();
			},

			renderItems : function () {
				var that = this;
				this.collection.each(function(model){
					var view = new ChartView ({"model" : model});
					that.$(that.selectors.chartsContent).append(view.render().$el)

				})
			},

			hide : function () {
				this.$el.hide();
			},

			show : function () {
				this.$el.show();
			}

		});
		
		console.log("ChartsView loaded");
		return ChartsView;
	});