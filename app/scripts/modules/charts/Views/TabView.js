define ([
	'backbone',
	'underscore',
	'common/Views/TabView'
	], function(Backbone, _, TabCommonView){
		var TabView = TabCommonView.extend({

			initialize : function () {
				this.addHandlers();
			},

			addHandlers : function () {
				this.listenTo(this.collection, "sync", this.renderNumber);
			},

			renderNumber : function () {
				this.$(this.selectors.number).html(this.collection.length);
			}

		});
		
		console.log("TabViewCharts loaded");
		return TabView;
	});