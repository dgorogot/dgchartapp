define ([
	'jquery',
	'backbone',
	'modules/charts/Views/ChartsView',
	'modules/charts/Views/TabView',
	'modules/charts/Collections/ChartsCollection'
	], function($, Backbone, ChartsView, TabView, ChartsCollection){
		var ChartsModule = Backbone.View.extend({
			views 		: {},
			collections : {},

			initialize : function () {
				this.initCollections();
				this.initViews();
				this.renderTab();
			},

			initCollections : function () {
				this.collections.chartsCollection = new ChartsCollection ();
			},

			initViews : function () {
				this.views.chartsView = new ChartsView ({
					el 		   : $('.charts-placeholder'),
					collection : this.collections.chartsCollection
				});
				this.views.tabView = new TabView ({
					collection : this.collections.chartsCollection
				});
			},

			renderTab : function () {
				var options = {
					"type"   : "home",
					"name"   : "home"
				};
				$('.aside-content').append(this.views.tabView.render(options).$el);
			},

			show : function () {
				this.collections.chartsCollection.fetch();
				this.views.tabView.highlight(true);
			},

			hide : function () {
				this.views.chartsView.hide();
				this.views.tabView.highlight(false);
			}

		});

		console.log("ChartModule loaded");
		return ChartsModule;
	});