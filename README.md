# DG Chart application #

### What about this application?###

This is SPA application which has been writing with backbone, highcharts, requirejs, LESS tchnologies. DG Charts application contain:

* Menu that has a link to the home page and to a page describing this application
* The home page should display a number of charts which loaded from 
* There is ability to display a larger chart version with descriptions in new page(each details chart has it's particular route)
* There is ability to alter name of the charts, by clicking edit chart button on details screen

Data for the application comes from "data" dirrectory, and stored in JSON files

### How start to work with application ###
* Clone this project into yours local mashine
* Install yeoman or simply just bower and grunt
* Once they have been installed go to the directory and install the grunt-cli -> 'npm install -g grunt-cli' and then 'npm install'
* Next run 'bower install' to install all the dependencies required
* Once everything has been installed simply run 'grunt server' and begin work with application
* Styles write in less files. In order to compile .less to .css run 'grunt less' command in your terminal